
#include "../header/header.h"
/////////////////////////////////////////////////////////////////

int Recv_Create_Msgq()
{
    int msqid;
    msqid=msgget((key_t)REC_MSG_KEY,IPC_CREAT|0666);
    if(msqid<0)
    {
        perror("ERROR:");
        return -1;
    }
    else
        return msqid;
}   
///////////////////////////////////////////////////////////////////////
void Recv_Write_MsgQue(int msqid,frame fr)
{
    msgbuf mb;
    if(fr.frame_type == CONTROL)
    mb.mtype = fr.frame_type;
    else if(fr.frame_type == DATA)
    mb.mtype = fr.frame_inst.frame_data.pos_id;
    mb.frame_fr =  fr;
    msqid=msgget((key_t)REC_MSG_KEY,IPC_CREAT|0666);
    msgsnd(msqid,(msgbuf *)&mb,sizeof(msgbuf),0);
}
//////////////////////////////////////////////////////////////////////
frame Recv_Read_MsgQue(int msqid,long mid)
{
    int n;
    frame fr;
    msgbuf mb;
    msqid=msgget((key_t)REC_MSG_KEY,IPC_CREAT|0666);
    msgrcv(msqid,(msgbuf *)&mb,sizeof(msgbuf),mid,0);
    fr = mb.frame_fr;
    return fr;
}   

