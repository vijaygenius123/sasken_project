#include "../header/header.h"

int Sem_Create()
{
    int semid;
    semid = semget((key_t) REC_SEM_KEY,2,IPC_CREAT|0666);
    if(semid < 0)
        {
            printf("Error creating semaphore");
            exit(0);
        }
    else
    printf("Sempaphore Created Sucessfully\n");
    return semid;
}

void Sem_Lock(int id)
{
    int semid;
    semid = semget((key_t) REC_SEM_KEY,2,IPC_CREAT|0666);
    sop.sem_num = 1;
    sop.sem_op = -1;
    sop.sem_flg = IPC_NOWAIT;
    printf("Transmitter Process #%d waiting\n", getpid());
    semop(semid, &sop, 1);
    printf("Transmitter Process #%d acquired. Sleeping\n", getpid());
    
}

void Sem_Unlock(int id)
{
        int semid;
        semid = semget((key_t) REC_SEM_KEY,2,IPC_CREAT|0666);
        printf("Transmitter Process #%d releasing\n", getpid());
        sop.sem_num = 1;
        sop.sem_op = 1;
        semop(semid, &sop, 1);
}
