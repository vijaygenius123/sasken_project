#include "../header/header.h"
//char *buff;

/* Usage
   logpipe_fd=LogPipe_Create()
   LogPipe_Write(char* buff)
   buff= Logpipe_Read()
   */
/********************************************
  CREATING LOGPIPE
 *********************************************/
int LogPipe_Create()
{
    int logpipe_fd;
    logpipe_fd  =   mkfifo(RECV_LOGPIPE,O_CREAT|0666);
    if(logpipe_fd<0)
    {
        perror("error:");
        return -1;
    }
    else
        return logpipe_fd;
}

/********************************************
  WRITING TO THE LOGPIPE
 *********************************************/
/*
   void LogPipe_Write(char* buff)
   {
   char buffer[100];
   int semid,r,c;
   strcpy(buffer,buff);
   semid=semget((key_t)0x03,1,IPC_CREAT|0666);  
   printf("semid=%d\n",semid);
   struct sembuf sb;
   semctl(semid,0,SETVAL,1);
   c=semctl(semid,0,GETVAL);
   printf("c before semop =%d\n",c);
   sb.sem_num=0;
   sb.sem_op=-1;
   r=semop(semid,&sb,1);              //locking the pipe using semaphore
   printf("r=%d\n",r);
   c=semctl(semid,0,GETVAL);
   printf("c after semop =%d\n",c);    
   int wfd=open("logpipe",O_WRONLY);         //opening the pipe                
   if(wfd==-1)
   perror("cant open");
   else{	
   printf("%d\n",wfd);
   write(wfd,buffer,sizeof(buffer));   //writing to the pipe

   printf("write to the pipe %s\n",buffer); 
   c=semctl(semid,0,SETVAL,1);
   c=semctl(semid,0,GETVAL);
   printf("c before exiting =%d\n",c);            
   }
   }
   */
/********************************************
  READING  THE LOGPIPE
 *********************************************/
/*
   char* LogPipe_Read()
   {
   int rfd,n;
   char buff[100];
   printf("%s\n","Im Here");
   rfd=open("logpipe",O_RDONLY);
   if(rfd<0)
   {
   printf("error opening a file");
   }
   printf("%s\n","I opened RFD");
   n = read(rfd,buff,sizeof(buff));                     //reading from the pipe
   printf("i Read %d \n",n);
   printf("%s\n",buff);
   return buff;
   }
   */
