#include "../header/header.h"
void error(const char *msg)
{
    perror(msg); 
    exit(1);
}
int Create_Server(void)
{
    int sockfd, newsockfd, portno,listenret;
    socklen_t clilen;
    struct sockaddr_in serv_addr, cli_addr;
    int n;
    sockfd = socket(AF_INET, SOCK_STREAM, 0);
    if (sockfd < 0) 
        error("ERROR opening socket");
    bzero((char *) &serv_addr, sizeof(serv_addr));
    serv_addr.sin_family = AF_INET;
    serv_addr.sin_addr.s_addr = INADDR_ANY;
    serv_addr.sin_port = htons(PORT_NO);
    if (bind(sockfd, (struct sockaddr *) &serv_addr,sizeof(serv_addr)) < 0) 
    {
        error("ERROR on binding");
        exit(-1);
    }
    else
    listenret = listen(sockfd,5);
    clilen = sizeof(cli_addr);
    newsockfd = accept(sockfd,(struct sockaddr *) &cli_addr,&clilen);
    if (newsockfd < 0) 
        error("ERROR on accept");
        return newsockfd;

}

frame Recieve_Socket_Frame(int newsockfd)
{        
    int n;
    frame fr;
    bzero((frame *)&fr,sizeof(frame));    
    n = read(newsockfd,(frame *)&fr,sizeof(frame));
    if (n < 0) error("ERROR reading from socket");
    return fr; 
}

void Send_Socket_Frame(int sockfd)
{
    frame fr;
    int n;
    fr = SendPipe_Read();
    n = write(sockfd,(frame *)&fr,sizeof(frame));   
    if (n < 0) 
        error("ERROR writing to socket");          
}

