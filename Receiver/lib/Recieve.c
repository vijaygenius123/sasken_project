#include "../header/header.h"
void Recieve(char *file_name,int pos_id,int file_count,int msqid,int Enc)        
{       
        frame fr,fa;
        int t=0;
        char buffer[PAYLOAD_SIZE];
        int line,fcs_check,len;
        FILE *file = fopen(file_name,"w+"); //open file in write mode
        for(line=0;line<file_count;)
        {
        bzero((frame *)&fr,sizeof(frame));
        fr = Recv_Read_MsgQue(msqid,pos_id);
        fcs_check = Check_FCS(fr);
        if(fcs_check == 1)
        {
        printf("Recieved Line %d Of %d Sucessfully \n ",line+1,file_count);
        if(Enc == 1)
        {
            printf("Decrypting\n");
            len = fr.frame_inst.frame_data.payload_len;
            strncpy(fr.frame_inst.frame_data.payload,Decryption(fr.frame_inst.frame_data.payload,len),len);
        }
        fwrite(fr.frame_inst.frame_data.payload,1,fr.frame_inst.frame_data.payload_len,file);
        fwrite("\n",1,sizeof(char),file);
        Print_Frame(fr);
        bzero((frame *)&fa, sizeof(frame));
        fa.frame_type = ACK;
        fa.frame_inst.frame_ack.ACK_KEY = AK;
        fa.frame_inst.frame_ack.pos_id = pos_id ;
        fa.frame_inst.frame_ack.payload_id = line ;
        line++;
        sleep(1);
        Sem_Lock(1);
        SendPipe_Write(fa);
        Sem_Unlock(1);
        }
        else
        {
        printf("Recieved Line %d Of %d Corrupted \n ",line+1,file_count);
        fa.frame_type = ACK;
        fa.frame_inst.frame_ack.ACK_KEY = NAK;
        fa.frame_inst.frame_ack.pos_id = pos_id;
        fr.frame_inst.frame_ack.payload_id = line;
        printf("NACK Sent\n");
        Print_Frame(fa);
        sleep(1);
        Sem_Lock(1);
        SendPipe_Write(fa);
        Sem_Unlock(1);
        }
        }
        fclose(file);
}

int Check_FCS(frame fr)
{
int i,sum=0;
for(i=0;i<PAYLOAD_SIZE;i++)
{
sum+=fr.frame_inst.frame_data.payload[i];
}
printf("Calculated FCS:%d\n",sum%256);
printf("From Frame:%d\n",fr.frame_inst.frame_data.fcs);
if(fr.frame_inst.frame_data.fcs == sum%256)
return 1;
else
return 0;
}
