#include "../header/header.h"

/*******************************************************
  CREATES A PIPE
 ********************************************************/
int SendPipe_Create(void)
{
    int pipe_fd;
    printf("In Snd Pipe Create\n");
    pipe_fd=mkfifo(RECV_ACKPIPE,O_CREAT|0666);    // Creating Send Pipe
    if(pipe_fd<0)
    {
        perror("error:");
        return -1;
    }
    else
        return pipe_fd;
}
/**********************************************************
  WRITES TO THE PIPE
 ***********************************************************/
void SendPipe_Write(frame fr)
{
    int wfd,r,c;
    wfd=open(RECV_ACKPIPE,O_WRONLY);                          //opening the pipe                
    if(wfd==-1)
        perror("cant open");
    else{	
        write(wfd,(frame *)&fr,sizeof(frame)); //writing to the pipe
    }

}


/**********************************************************
  READS FROM PIPE
 ***********************************************************/
frame SendPipe_Read(void)
{

    int rfd,n;
    frame fr;
    rfd=open(RECV_ACKPIPE,O_RDONLY);
    //printf("opening pipe done\n");
    if(rfd==-1)
        perror("cant open");
    n = read(rfd,(frame *)&fr,sizeof(fr));
    printf("Read From Pipe File %d\n",n);
    return fr;
}




