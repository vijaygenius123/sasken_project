#include"../header/header.h"

pid_t pid_array[MAX_ARRAY_SIZE],Rcv_array[MAX_ARRAY_SIZE];
int main(void)
{
    int ret,spfd,lpfd,sockfd,msqid,mid,RecvNum,i,j,Enc = 0;
    pid_t mypid;
    int line_count[MAX_ARRAY_SIZE];
    frame fr,fa;
    char file_names[MAX_ARRAY_SIZE][MAX_FN_LENGTH];
    //////////////////////// Main Creates Server Socket //////////////////

    sockfd = Create_Server();    // Create Server Socket 
    mypid = getpid();  // Get Transmitter Process PID (Parent)
    ///////////////////////////   Create ACK Message Quee  //////////////
    msqid = Recv_Create_Msgq();
    if(msqid == -1)
    {
        printf("Message Quee Not Created\n");
        exit(-1);
    }
    ///////////////////////////   Create Pipe To Send Frames ///////////
    spfd=SendPipe_Create();
    if(spfd == -1)
    {
        printf("Send Pipe Already Exists\n");
    }
    /////////////////////////////  Create Log Pipe  /////////////////////
    lpfd=LogPipe_Create(); 
    if(lpfd == -1)
    {
        printf("Log Pipe Already Exists\n");
    }
    //////////////////////////////  Spawn Logger Process /////////////////
    if(mypid == getpid())
    {
        ret = Spawn_Logger_Process(pid_array);
        if(ret == -1)
        {
            printf("Logger Not Spawned \n");
            exit(-1);
        }
    }
    ///////////////////////////// Spawn S&R Process //////////////////
    if(mypid == getpid())
    {
        ret = Spawn_SR_Process(pid_array);
        if(ret == -1)
        {
            printf("S&R Process Not Spawned \n");
            exit(-1);
        }
    }
    /////////////////////// Schedule Logger Process   //////////////////
    if(pid_array[0] == 1)
    {
        printf("Logger Scheduled\n");
        printf("Logger PID %d\n",getpid());
    }
    if(mypid == getpid())
    {
        sleep(1);
        bzero((frame *)&fr, sizeof(frame));
        fr = Recv_Read_MsgQue(msqid,1);
        RecvNum = fr.frame_inst.frame_ctrl.file_count;
        //printf("RecvNum:%d\n",RecvNum);
        //printf("File Names:%s Lines%d\n",fr.frame_inst.frame_ctrl.file_names[0],fr.frame_inst.frame_ctrl.file_lines[0]); 
        for(i=0;i<RecvNum;i++)
        {
            printf("File Names:%s Lines:%d\n",fr.frame_inst.frame_ctrl.file_names[i],fr.frame_inst.frame_ctrl.file_lines[i]);
            line_count[i] = fr.frame_inst.frame_ctrl.file_lines[i]; 
            strcpy(file_names[i],fr.frame_inst.frame_ctrl.file_names[i]); 
        }
        Enc = fr.frame_inst.frame_ctrl.encrypt;
        bzero((frame *)&fa,sizeof(frame));
        fa.frame_type = ACK ;
        fa.frame_inst.frame_ack.ACK_KEY = AK;
        fa.frame_inst.frame_ack.pos_id =  1;
        Print_Frame(fa);
        SendPipe_Write(fa);
    }
    ///////////////////////  S&R Process Scheduled   ///////////////////
    if(pid_array[1] == 2)
    {   
        printf("S&R Scheduled\n");
        //printf("S&R PID %d\n",getpid());
        while(1)
        { 
        fr = Recieve_Socket_Frame(sockfd);  // Recieve Data From Socket
        Recv_Write_MsgQue(msqid,fr);
        Send_Socket_Frame(sockfd);              
        }
    }
    //////////////////////////  Spawn Reciever 1 ////////////////////////////   
    if(mypid == getpid())
    {
        if(2<=(RecvNum+1))
        Spawn_Reciever_Process(pid_array,2);
        
    }
    //////////////////////////  Spawn Reciever 2 ////////////////////////////        
    if(mypid == getpid())
    {
        if(3<=(RecvNum+1))
        Spawn_Reciever_Process(pid_array,3);
    }
    //////////////////////////  Spwan Reciever 3 ///////////////////////////      
    if(mypid == getpid())
    {
        if(4<=(RecvNum+1))
        Spawn_Reciever_Process(pid_array,4);
    }
    //////////////////////////   Spawn Reciever 4 ///////////////////////////
    if(mypid == getpid())
    {
        if(5<=(RecvNum+1))
        Spawn_Reciever_Process(pid_array,5);
    }

    ////////////////// Schedule Reciever 1 //////////////////////////
    if(pid_array[2] == 3)
    {
        printf("Reciever 1 With PID %d\n",getpid());
        Recieve(file_names[0],2,line_count[0],msqid,Enc);
    }
    ////////////////// Schedule Reciever 2 //////////////////////////
    if(pid_array[3] == 4)
    {
        printf("Reciever 2 With PID %d\n",getpid());
        Recieve(file_names[1],3,line_count[1],msqid,Enc);
    }
    ////////////////// Schedule Reciever 3 //////////////////////////
    if(pid_array[4] == 5)
    {
        printf("Reciever 3 With PID %d\n",getpid());
        Recieve(file_names[2],4,line_count[2],msqid,Enc);
    }
    ////////////////// Schedule Reciever 4 //////////////////////////
    if(pid_array[5] == 6)
    {
        printf("Reciever 4 With PID %d\n",getpid());
        Recieve(file_names[3],5,line_count[3],msqid,Enc);
    }

    /////////////////////// Wait For All Child To Exit///////////////

    if(mypid == getpid())
    {
        printf("Wait For All Childs To Exit\n");
        for(i=0;i<RecvNum+1;i++)
        {
        wait();
        }
        close(sockfd);
        kill(0,SIGKILL); 
    }
    return 0;
}
