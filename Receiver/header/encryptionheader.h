//string initialization used to find position of the alphabet for lower case
char standard_alpha[30]="abcdefghijklmnopqrstuvwxyz"; 

//string initialization used to find position of the alphabet for Upper case 
char standard_uppercase[30]="ABCDEFGHIJKLMNOPQRSTUVWXYZ";

//string initialization used to find the index value of the special symbol
char standard_special[35]="!\"#$%&\'()*+,-./:;<=>?@[\\]^_`{|}~";

//string initialization used to find index value for digit 
char standard_digit[10]="0123456789";  

//Lookup table for Special syombols encryption
char *standard_symbol[]={

":;<=>?@[\\]^_`{|}~!\"#$%&\'()*+,-./",
"{|}~!\"#$%&\'()*+,-./:;<=>?@[\\]^_`"
"! \"#$%&\'()*+,-./:;<=>?@[\\]^_`{|}~",
};

//Lookup table for Upper case alphabets encryption
char *standard_upper[]={
"PQRSTUVWXYZABCDEFGHIJKIMNO",
"VWXYZABCDEFGHIJKLMNOPQRSTU",
"GHIJKLMNOPQRSTUVWXYZABCDEF",
"KLMNOPQRSTUVWXYZABCDEFGHIJ",
"OPQRSTUVWXYZABCDEFGHIJKLMN",
"TUVWXYZABCDEFGHIJKLMNOPQRS",
"IJKLMNOPQRSTUVWXYZABCDEFGH",
"DEFGHIJKLMNOPQRSTUVWXYZABC",
"XYZABCDEFGHIJKLMNOPQRSTUVW",
"RSTUVWXYZABCDEFGHIJKLMNOPQ"
};

//Lookup table for Lower case Letter encryption
char *standard_alpencrypt[]={
"pqrstuvwxyzabcdefghijklmno",
"vwxyzabcdefghijklmnopqrstu",
"ghijklmnopqrstuvwxyzabcdef",
"klmnopqrstuvwxyzabcdefghij",
"opqrstuvwxyzabcdefghijklmn",
"tuvwxyzabcdefghijklmnopqrs",
"ijklmnopqrstuvwxyzabcdefgh",
"defghijklmnopqrstuvwxyzabc",
"xyzabcdefghijklmnopqrstuvw",
"rstuvwxyzabcdefghijklmnopq"};

//Lookup table for digits encryption
char *standard_digitencrypt[]={"5678901234","8901234567","2345678901"};


