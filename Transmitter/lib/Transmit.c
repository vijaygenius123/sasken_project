#include "../header/header.h"

frame fr,fa;
int Retry = 3;
int t;
void Transmit(char* filename,int pos_id,int Enc)
{
FILE *file = fopen(filename,"r");
int msqid,len,blk=0;
char buffer[100][80],ch;
int i=0,j=0,line_count;
line_count = 0;
 do 
{
    ch = fgetc(file);
        if(ch == '\n')
        {
            line_count++;
            j++;
            i=0;
        }
       else
       {
            buffer[j][i]=ch;
            i++;  
       }
} 
while (ch != EOF);
for(i=0;i<line_count;)
{
bzero((frame *)&fr,sizeof(frame));
fr.frame_type = DATA;
if(Enc == 1)
{
    len = 0;
    printf("Encrypting Payload\n");
    len = strlen(buffer[i]);  
    fr.frame_inst.frame_data.payload_len = len;
    strncpy(fr.frame_inst.frame_data.payload,Encrypt_Frame(buffer[i],len),len);
}
else
{
len = strlen(buffer[i]);  
fr.frame_inst.frame_data.payload_len = len;
strncpy(fr.frame_inst.frame_data.payload,buffer[i],len);
}
fr.frame_inst.frame_data.pos_id = pos_id;
fr.frame_inst.frame_data.payload_id = i;
fr.frame_inst.frame_data.fcs =  Calc_FCS(fr);
Sem_Lock(1);
SendPipe_Write(fr);
Sem_Unlock(1);
bzero((frame *)&fa,sizeof(frame));
t = 0;
//signal(SIGALRM, alarm_handler);
//alarm(MAX_TIMEOUT);
fa = Trans_Read_MsgQue(pos_id);
while(!t)
{
    fa = Trans_Read_MsgQue_NB(pos_id);
    if(fa.frame_type == ACK)
    {  
    alarm(0);
    printf("***************************Im HERE********************************\n");
    if(fa.frame_inst.frame_ack.ACK_KEY == AK)
    t = 2;
    else if(fa.frame_inst.frame_ack.ACK_KEY == NAK)
    t = 3;
    }
}
if(t == 2)
{
Retry = 3;
printf("Ack Recieved %d For %d \n",pos_id,i+1);
i = fa.frame_inst.frame_ack.payload_id + 1;
}
else
{
if(Retry >= 0)
{
printf("NACK Recieved Or Timer Expired  Resending Same Frame\n");
Retry--;
}
}
}
}

int Calc_FCS(frame fr)
{
int i,sum=0;
for(i=0;i<PAYLOAD_SIZE;i++)
{
sum+=fr.frame_inst.frame_data.payload[i];
}
printf("FCS:%d\n",sum%256);
return sum%256;
}

void alarm_handler(int num)
{
    alarm(0);
	t=1;
	printf("Timer Expired\n");
}
