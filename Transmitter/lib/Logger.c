#include "../header/header.h"

FILE *file;
void Log_Open(void)
{
FILE *file = fopen("Tx_Log.txt","w");
}


void Log_Write(char *buff)
{
char data[100];
strcpy(data,buff);
Sem_Lock(0);
LogPipe_Write(data);
Sem_Unlock(0);
}

void Log_Read(void)
{
char data[100];
char *buff;
time_t timer;
char buffer[26];
struct tm* tm_info;
time(&timer);
tm_info = localtime(&timer);
strftime(buffer, 26, "%Y:%m:%d %H:%M:%S", tm_info);
bzero((char *)&data,sizeof(data));
buff = LogPipe_Read();
strncpy(data,buff,strlen(buff));
printf("Log Pipe In %s\n",buff);
//fprintf(file,"%s \t %s\n",buffer,LogPipe_Read());
}

void Log_Close(void)
{
fclose(file);
}
