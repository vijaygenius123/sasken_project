#include "../header/header.h"

/* Usage
   logpipe_fd=LogPipe_Create()
   LogPipe_Write(char* buff)
   buff= Logpipe_Read()
   */
/*********************************************
  CREATING LOGPIPE
 *********************************************/
int LogPipe_Create()
{
    int logpipe_fd;
    logpipe_fd  =   mkfifo(TRANS_LOGPIPE,O_CREAT|0666);
    if(logpipe_fd<0)
    {
        perror("error:");
        return -1;
    }
    else
        return logpipe_fd;
}
/********************************************
  WRITING TO THE LOGPIPE
 *********************************************/
void LogPipe_Write(char* inbuff)
{
    char buffer[100];
    strcpy(buffer,inbuff);    
    int wfd=open(TRANS_LOGPIPE,O_WRONLY);         //opening the pipe                
    if(wfd==-1)
        perror("cant open");
    else{	
        //printf("%d\n",wfd);
    write(wfd,(char *)&buffer,sizeof(buffer));   //writing to the pipe   
    printf("LogPipe Data In %s\n",buffer);         
    }
}
/********************************************
  READING  THE LOGPIPE
 *********************************************/
char* LogPipe_Read()
{
    int rfd,n;
    char buff[100];
    rfd=open(TRANS_LOGPIPE,O_RDONLY);
    if(rfd<0)
    {
        printf("error opening a file");
    }
    n = read(rfd,(char *)&buff,sizeof(buff));                     //reading from the pipe
    return buff;
}
