#include<stdio.h>
#include <sys/types.h>
#include <unistd.h>
#include "../header/header.h"


int Create_Child()
{
    pid_t mypid;
    mypid = fork();
        return mypid;
}
/********************************
  Creates Send And Receivce Process On Success Stores Logger PID To Global Array pid_array
  Else Returns -1 To Transmitter 
 *********************************/

int Spawn_SR_Process(pid_t pid_array[]){
    int ret;
    pid_array[1] = Create_Child(); //Create S&R Process
    if(pid_array[1] == 0)
    {
        //printf("S&R Process Created With PID %d\n",getpid());
        pid_array[1] = 2;
    }
    if(pid_array[1] == -1)
    {
        printf("Logger Process Not Created\n");
        return -1;
    }
}
/********************************
  Creates Logger Process On Success Stores Logger PID To Global Array pid_array
  Else Returns -1 To Transmitter 
 *********************************/
int Spawn_Logger_Process(pid_t pid_array[]){
    pid_array[0] = Create_Child(); // Create Logger Process
    if(pid_array[0] == 0)
    {
        //printf("Logger Process Created With PID %d\n",getpid());
        pid_array[0] = 1;
    }
    if(pid_array[0] == -1)
    {
        printf("Logger Process Not Created\n");
        return -1;
    }
}
/********************************
  Creates Transmitter Process On Success Stores Logger PID To Global Array pid_array
  Else Returns -1 To Transmitter 
 *********************************/
int Spawn_Transmitter_Process(pid_t pid_array[],int i)
{
    int  j;
    pid_array[i] = Create_Child(); // Create Logger Process
    if(pid_array[i] == 0)
    {
           // printf("Transmitter %d Process Created With PID %d\n",i-1,getpid());
            pid_array[i] = i+1;
    }
    if(pid_array[i] == -1)
    {
        printf("Transmitter Process Not Created\n");
        return -1;
    }
}
