/*
Author:Karuna 
 **************************************************************************************************
 Program for Encryption of text taken from the payload
 **************************************************************************************************
//*************************Header files inclusion********************************************/
#include "../header/encryptionheader.h"
#include "../header/header.h"

char payload_out[PAYLOAD_SIZE];
/*char* Encrypt_Frame(char *payload,int len)
{
    char *encrypted_payload;
    //int *Pos;
   encrypted_payload=Encrypt(payload,len);
    //= Encrypt_Lev2(Pos,len);
    return encrypted_payload;
}
*/
//**************************function to encrypt the line Level 1**********************************//
char* Encrypt_Frame(char *payload,int len)
{
    int z=0,up,elesp,j=0,i,sp=0,ele;
    
    char *b; 
    //char payload[PAYLOAD_SIZE];
    int pos[PAYLOAD_SIZE];
    for(j=0;j<len;j++)	  
    {
        if(isdigit(payload[j]))
        {
            for(i=0;i<10;i++)
            {
                if(payload[j]==standard_digit[i])    
                {
                    pos[j]=i;
                }
            }
        }
        else if(payload[j]==' ')
        {
            pos[j]=32;
        }
        else if(isalpha(payload[j]))
        {
            //*******To find position value in english alphabet(lower case)*******//
            if(payload[j]>='a'&& payload[j]<='z')
            {
                for(i=0;i<26;i++)
                {
                    if(payload[j]==standard_alpha[i])    
                    {	
                        pos[j]=i;
                    }
                }
            }
//***********To find the position value of alphabet(upper case)*************//   
            else if(payload[j]>='A'&& payload[j]<='Z')
            {
                for(i=0;i<26;i++)
                {
                    if(payload[j]>='A'&& payload[j]<='Z')
                    {
                        if(payload[j]==standard_uppercase[i])    
                        {
                            pos[j]=i;
                        }
                    }
                }
            }
        }
//**************To find the position value of special characters******************//
        else {
            for(sp=0;sp<32;sp++)
            {
                if(payload[j]==standard_special[sp])
                {
                    pos[j]=sp;
                }
            }
        }

    }
//    return pos;
//**************************Level 2******************************************//         
    for(ele=0;ele<len;ele++)
    {
        //printf("pos[%d] = %d \n",ele,pos[ele]);     
        if(payload[ele]==' ')
        {
            //printf("%c",32);
            payload_out[ele]=' ';
        }
//*************************If the element is an alphabet***************************//
        else if((isalpha(payload[ele])))
        {
            //printf("Alphabet\n");
            z=z%10;	
            if(islower(payload[ele]))
                b = standard_alpencrypt[z];
            else
                b = standard_upper[z];   
                payload_out[ele]=b[pos[ele]];
                //printf("%c",payload_out[ele]);
            z++;	
        }
        //***********************If the element is a digit*********************//
        else if(isdigit(payload[ele]))
        {
            //printf("Digit\n");
            j=j%3;		
            b=standard_digitencrypt[j];
            payload_out[ele]=b[pos[ele]];
            //printf("%c",payload_out[ele]);
            j++;
        }
//******************If the element is a special character**********************//
        else 
        {
            //printf("Special\n");
            sp=sp%3;
            b=standard_symbol[sp];
            payload_out[ele]=b[pos[ele]];
            //printf("%c",payload_out[ele]);
            sp++;	
        }
    }
    //////////////printf("\n");
    return payload_out;
}

