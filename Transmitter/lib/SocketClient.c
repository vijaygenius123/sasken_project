
#include "../header/header.h"

void error(char* msg)
{
    perror(msg);
    exit(1);
}

int Create_Socket()
{
    int sockfd, portno, n,i;
    frame fr;
    struct sockaddr_in serv_addr;
    struct hostent *server;
    sockfd = socket(AF_INET, SOCK_STREAM, 0);
    if (sockfd < 0) 
        error("ERROR opening socket");
    server = gethostbyname(IP);
    if (server == NULL) {
        fprintf(stderr,"ERROR, no such host\n");
        exit(0);
    }
    bzero((char *) &serv_addr, sizeof(serv_addr));
    serv_addr.sin_family = AF_INET;
    bcopy((char *)server->h_addr, 
            (char *)&serv_addr.sin_addr.s_addr,
            server->h_length);
    serv_addr.sin_port = htons(PORT_NO);
    if (connect(sockfd,(struct sockaddr *) &serv_addr,sizeof(serv_addr)) < 0) 
        error("ERROR connecting");
    else
        return sockfd;
}

void Send_Socket_Frame(int sockfd)
{
    frame fr;
    int n;
    bzero((frame *)&fr,sizeof(fr));
    fr = SendPipe_Read();
    n = write(sockfd,(frame *)&fr,sizeof(frame));
    Print_Frame(fr);   
    if (n < 0) 
        error("ERROR writing to socket");      
}

frame Recieve_Socket_Frame(int sockfd)
{        
    int n;
    frame fr;
    bzero((frame *)&fr,sizeof(fr));
    n = read(sockfd,(frame *)&fr,sizeof(fr));
    Print_Frame(fr);
    if (n < 0) error("ERROR reading from socket");
    return fr; 
}
