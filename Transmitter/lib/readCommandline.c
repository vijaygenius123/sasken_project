#include "../header/header.h"
frame Read_Command_Line(int c,char* v[])
{ 
    int i=0,j=0;
    int encrypt,line_count;
    char file_name[10],ch;
    encrypt = atoi(v[1]);
    frame fr;
    bzero((frame *)&fr, sizeof(frame));
    fr.frame_type= CONTROL;
    printf("Encrypt = %d\n",encrypt);
    if(c==1)
    {
        printf("usage transmitter encrip_flag filename<s>.txt\n");
        exit(-1); 
    } 
    else if(c<3)
    {
        printf("usage transmitter encrip_flag filename<s>\n"); 
    } 
    else
    { 
        if( encrypt < 0 || encrypt > 2 ) // validate the encrypt flag 
        {
            printf("Invalid encrypt flag. The flag should be either 0 or 1 \n"); 
        }
        if(atoi(v[1])==1)
        {
            fr.frame_inst.frame_ctrl.encrypt = 1;   
        }
        else
            fr.frame_inst.frame_ctrl.encrypt = 0; 
        fr.frame_inst.frame_ctrl.file_count = c - 2;
        for(i=2; i<(c); i++)
        {

            FILE *file = fopen(v[i],"r"); //check for the text file 
            if(file==NULL)
            {
                printf( "Could not open file \n" );
                exit(1);
            }
            else
            {
                line_count = 0;
                do 
                {
                    ch = fgetc(file);
                    if(ch == '\n')
                        line_count++;
                } 
                while (ch != EOF);

                //printf("v[%d]=%s\n",i,v[i]);
                printf("Lines In %s is %d\n",v[i],line_count);
                strcpy(fr.frame_inst.frame_ctrl.file_names[j],v[i]); // update the filename array only with filenames
                fr.frame_inst.frame_ctrl.file_lines[j] = line_count;
                j++;
                fclose(file);  
            } 
        }     

    }
    return fr;
}



