
#include "../header/header.h"
/////////////////////////////////////////////////////////////////

int Trans_Create_Msgq()
{
    int msqid;
    msqid = msgget((key_t)TR_MSG_KEY,IPC_CREAT|0666);
    if(msqid<0)
    {
        perror("ERROR:");
        return -1;
    }
    else
        return msqid;
}
///////////////////////////////////////////////////////////////////////
void Trans_Write_MsgQue(frame fr)
{
    msgbuf mb;
    //mb.mtype = fr.frame_type;
    ///mb.frame_fr =  fr;
    if(fr.frame_type ==  ACK)
    {
        printf("ACK Recieved\n");
        mb.mtype = fr.frame_inst.frame_ack.pos_id;
        mb.frame_fr = fr;
    }
    msqid=msgget((key_t)TR_MSG_KEY,IPC_CREAT|0666);
    msgsnd(msqid,(msgbuf *)&mb,sizeof(msgbuf),0);
}
//////////////////////////////////////////////////////////////////////
frame Trans_Read_MsgQue(long mid)
{
    int n;
    frame fr;
    msgbuf mb;
    msqid=msgget((key_t)TR_MSG_KEY,IPC_CREAT|0666);
    msgrcv(msqid,(msgbuf *)&mb,sizeof(msgbuf),mid,0);
    fr = mb.frame_fr;
    return fr;
}   
frame Trans_Read_MsgQue_NB(long mid)
{
    int n;
    frame fr;
    msgbuf mb;
    msqid=msgget((key_t)TR_MSG_KEY,IPC_CREAT|0666);
    msgrcv(msqid,(msgbuf *)&mb,sizeof(msgbuf),mid,IPC_NOWAIT);
    fr = mb.frame_fr;
    return fr;
} 
