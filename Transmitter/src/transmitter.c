#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>   
#include "../header/header.h"

pid_t pid_array[MAX_ARRAY_SIZE];
int main(int argc,char *argv[])
{
    /////////////////// Variables ///////////////////////////
    int ret,spfd,lpfd,msqid,sockfd,i,j,semid,RecvReady=0,Enc;
    FILE *fp;
    pid_t mypid;
    frame fr,fa;

    /////////////////////   Shared Resources   ///////////////////   


    sockfd = Create_Socket();
    mypid = getpid();  // Get Transmitter Process PID (Parent)
    /////////////////// Semaphore //////////////////////////
    
    semid = Sem_Create();
    ///////////////////// Message Queue ///////////////////
    msqid = Trans_Create_Msgq();
    if(msqid == -1)
    {
        printf("Message Quee Not Created\n");
        exit(-1);
    }
    spfd=SendPipe_Create();
    if(spfd == -1)
    {
        printf("Send Pipe Already Exists\n");
    }
    lpfd=LogPipe_Create(); 
    if(lpfd == -1)
    {
        printf("Log Pipe Already Exists\n");
    }
    ///////////////////////  Spawn Logger Process /////////////////
    if(mypid == getpid())
        ret = Spawn_Logger_Process(pid_array);
    if(ret == -1)
    {
        printf("Logger Not Spawned \n");
        exit(-1);
    }

    ///////////////////////  Spawn SR Process /////////////////
    if(mypid == getpid())
        ret = Spawn_SR_Process(pid_array);
    if(ret == -1)
    {
        printf("S&R Process Not Spawned \n");
        
        exit(-1);
    }
    /////////////// Schedule Logger Process ////////////////////
    if(pid_array[0] == 1)
    {
    //Log_Open();
    //while(1)
    //{
    //Log_Read();
    //}
    }
    /////////////// Schedule SR Process ////////////////////////
    if(pid_array[1] == 2)
    {   
        //printf("S&R Scheduled\n");
        //Log_Write("S&R Scheduled");
        while(1)
        {
        Send_Socket_Frame(sockfd);
        fa = Recieve_Socket_Frame(sockfd);
        Trans_Write_MsgQue(fa);
        }
    }
    if(mypid == getpid())
    {
        sleep(1);   
        fr = Read_Command_Line(argc,argv);
        Enc = atoi(argv[1]);
        SendPipe_Write(fr);
        fa = Trans_Read_MsgQue(1);
        if(fa.frame_type == ACK)
        {
        if(fa.frame_inst.frame_ack.ACK_KEY == 1)
        RecvReady = 1;
        }
        else
        {
        printf("Reciever Not Ready\n");
        exit(1);
        }
        
    }
    //////////////////////////  Spawn Transmitter 1 ////////////////////////////   
    if(mypid == getpid() && RecvReady == 1)
    {
        if(2<=(argc-1))
        Spawn_Transmitter_Process(pid_array,2);
    }
    //////////////////////////  Spawn Transmitter 2 ////////////////////////////        
    if(mypid == getpid()&& RecvReady == 1)
    {
        if(3<=(argc-1))
        Spawn_Transmitter_Process(pid_array,3);
    }
    //////////////////////////  Spwan Transmitter 3 ///////////////////////////      
    if(mypid == getpid()&& RecvReady == 1)
    {   
        if(4<=(argc-1))
        Spawn_Transmitter_Process(pid_array,4);
    }
    //////////////////////////   Spawn Transmitter 4 ///////////////////////////
    if(mypid == getpid()&& RecvReady == 1)
    {
        if(5<=argc-1)
        Spawn_Transmitter_Process(pid_array,5);
    }
    ////////////////// Schedule Transmitter 1 //////////////////////////
    if(pid_array[2] == 3)
    {
        printf("Transmitter 1 With PID %d\n",getpid());
        Transmit(argv[2],2,Enc);
    }
    ////////////////// Schedule Transmitter 2 //////////////////////////
    if(pid_array[3] == 4)
    {
        printf("Transmitter 2 With PID %d\n",getpid());
        Transmit(argv[3],3,Enc);
    }
    ////////////////// Schedule Transmitter 3 //////////////////////////
    if(pid_array[4] == 5)
    {
        printf("Transmitter 3 With PID %d\n",getpid());
        Transmit(argv[4],4,Enc);
    }
    ////////////////// Schedule Transmitter 4 //////////////////////////
    if(pid_array[5] == 6)
    {
        printf("Transmitter 4 With PID %d\n",getpid());
        Transmit(argv[5],5,Enc);
    }

    /////////////////////// Wait For All Child To Exit///////////////
   if(mypid == getpid())
    {
       printf("Wait For All Childs To Exit\n");
        for(i=0;i<argc-2;i++)
        {
        wait();
        }
        close(sockfd);
        kill(0,SIGKILL); 
    }

    return 0;
}
