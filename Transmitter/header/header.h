#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/msg.h>
#include <errno.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/sem.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <signal.h>
#include <sys/time.h>
#include <ctype.h>
#include <time.h>

#define IP "127.0.0.1"
#define PORT_NO 5500

#define MAX_DATA 100
#define MAX_FN 10     //Maximum no of files
#define MAX_FN_LENGTH 30   // Max filename length
#define PAYLOAD_SIZE 80   //Payload size is fixed as 80 bytes.
#define MAX_TIMEOUT 3    //Time in seconds for unreceived frame.
#define MAX_ARRAY_SIZE 10

#define TRANS_SNDPIPE "TRANS_SEND_PIPE"  // Transmitter Send PIPE Name
#define TRANS_LOGPIPE "TRANSLOG_PIPE"   // Transmitter Log PIPE Name
#define RECV_ACKPIPE "RECV_ACK_PIPE"   //  Reciever ACK Pipe Name
#define RECV_LOGPIPE "RECV_LOG_PIPE"  //  Reciever Log Pipe Name 

#define AK 1
#define NAK 0

typedef enum type {CONTROL=1,DATA=2,ACK=3} type ; // This enum type is used to check the frame type(Control frame, Data Frame or Acknowledgement Frame)

//Frame Template used in the frame. Contains the data contained in each line of the File. 
/***********************Data Frame**********************/
typedef struct data1 {						
    char payload[PAYLOAD_SIZE];	
    int payload_len;			
    int pos_id;
    int fcs;
    int payload_id;
}data;


/***********************Acknowledgement Frame**********************/

typedef struct ack1 {
    int ACK_KEY;
    int pos_id;
    int payload_id;
}ack;

/***********************Control Frame**********************/
typedef struct ctrl1 {
    int file_count;
    int encrypt;
    int file_lines[MAX_FN];
    char file_names[MAX_FN][MAX_FN_LENGTH];
}ctrl;

/*******************************Structure of the frame ********************************************/

typedef union frame_union1{
    data frame_data;
    ctrl frame_ctrl;
    ack frame_ack;
}frame_union;

typedef struct frame1{			 	 
    type frame_type;		//Type of the frame is identified here.
    frame_union frame_inst;	       //Instance of the frame created here.
}frame;	

typedef struct mymsgbuf {
               long mtype;       /* message type, must be > 0 */
               frame frame_fr;    /* message data */
           }msgbuf;

struct sembuf sop;
/*************************************************************
  global's for Transmitter and Receiver
 *************************************************************/
//int pid_array[MAX_ARRAY_SIZE];
int ppid;
int msqid;

char fileName[MAX_FN_LENGTH];

extern int tr_logger_fd;
extern int rec_logger_fd;

#define TR_SEM_KEY	0x01    // Transmitter Side Semaphore Key
#define TR_MSG_KEY	0x01    // Transmitter Side Semaphore Key

#define REC_MSG_KEY	0x02    // Reciever Side Message Key
#define REC_SEM_KEY	0x02    // Receiver Side Semaphore Key


/*************************************************************
  Function Prototype for Transmitter Side
 *************************************************************/
frame Read_Command_Line(int,char**);
void Create_Frame(char *);

int Create_Fcs(char []);
void Transmitter_Socket(void);
void Sender_Pipe(void);
void Sender_Message_Queue(void);
int Create_Processes(void);
int Stop_Timer(void);     
int Return_Timer(void);
int Start_Timer(void);
int Getfile_Count(void);
void Process_Task( char *);
pid_t Create_Child(void);
int MessageQuee_Create(void);
ack MessageQuee_Read(ack);
void MessageQuee_Write(ack);
int SendPipe_Create(void);
frame SendPipe_Read(void);
void SendPipe_Write(frame);
int LogPipe_Create(void);
void LogPipe_Write(char*);
char* LogPipe_Read(void);
void Open_Log(void); 
void Logger(void);
///////////////// Encryption //////////////////
char* Encrypt_Frame(char [],int);
//int* Encrypt_Lev1(char *,int);
//char* Encrypt_Lev2(int *,int);
//char* Encrypt(char,int)
////////////////////// Timer ////////////////////
void alarm_handler(int);

/////////////////////Semaphore//////////////////
int Sem_Create(void);
void Sem_Lock(int);
void Sem_Unlock(int);

////////////////// Transmit //////////////
void Transmit(char*,int,int);
int Calc_FCS(frame);

///////////////////// Spawning ///////////////
pid_t Spawn_Logger_Process(pid_t []);    // Creates The Loggger Process 
pid_t Spawn_SR_Process(pid_t []);    // Creates The Loggger Process 
pid_t Spawn_Transmitter_Process(pid_t [],int);

/////////// Frame ///////////
frame Create_Ctrl_Frame(int , char**);

//////////// Socket //////////////////////////////
void Send_Socket_Frame(int);
int Create_Socket(void);
frame Recieve_Socket_Frame(int);
/////////////// Message Quee //////////
int Trans_Create_Msgq(void);
void Trans_Write_MsgQue(frame);    // Write To Message Queue
frame Trans_Read_MsgQue(long);      // Read From Message Queue
frame Trans_Read_MsgQue_NB(long);
/*************************************************************
  Function Prototype for Receiver Side
 *************************************************************/

void Recv_Socket_Time(int);
int Recv_Create_Process(int,char *);
int Recv_Create_Pipe(char *);
int Recv_Create_Msgq(void);
void Recv_Read_Pipe(int);
void Recv_Write_Pipe(int);
void Recv_Read_Msgq(int, char *);
void Recv_Write_Msgq(int, char *);
void Recv_Decrypt(char *);
void Recv_Ack_Timer(int);
void Check_Frame_Struct ();
int RecCreateMsgQue();

void Print_Frame(frame);
